<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once 'model.php';
CModule::IncludeModule("iblock");
CModule::IncludeModule("user");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;



/*var_dump($FIRSTNAME1 );
var_dump($MIDDLENAME1);
var_dump($LATNAME1);
var_dump($BIRTHDAY1);
var_dump($DATE_VISIT1);
*/
$getHL = new evraz3;
$inputDate = $_POST["99999date_visit"]; //берем из инпут дата посещения

$toDelete = 3;
mb_internal_encoding("UTF-8");
$result2 = mb_substr($inputDate, $toDelete);

class evraz3
{


    public function inHL2($ID, $filter) {
       // $filter = array('?UF_FIRSTNAME' => 'Устюжанина');
        $hlblock = HL\HighloadBlockTable::getById($ID)->fetch(); // тут получаем хайблок
        $entity = HL\HighloadBlockTable::compileEntity($hlblock); // тут получаем данные о данном хайблоке
        $entity_data_class = $entity->getDataClass(); // фича для записи
        $rsData = $entity_data_class::getList(array(
            'select' => array('*'),
            'filter' => $filter
        ));
        $request = [];

        while($el = $rsData->fetch()){
            array_push($request, $el);
        }
//var_dump($request);
        return $request;
    }


/*
    public function inHL2($ID)
    {

        $hlblock = HL\HighloadBlockTable::getById($ID)->fetch(); // тут получаем хайблок
        $entity = HL\HighloadBlockTable::compileEntity($hlblock); // тут получаем данные о данном хайблоке
        $entity_data_class = $entity->getDataClass(); // фича для записи
        $rsData = $entity_data_class::getList(array(
            'select' => array('ID','UF_FIRSTNAME', 'UF_MIDDLENAME', 'UF_LASTNAME', 'UF_BIRTHDAY'),
            //'filter' => array('?UF_FIRSTNAME' => 'Устюжанина', '?UF_MIDDLENAME' => 'Ольга', '?UF_LASTNAME' => 'Николаевна', '?UF_BIRTHDAY' => '24.02.1993')
        ));

        $request = [];

        while ($el = $rsData->fetch()) {

            array_push($request, $el);

        }
        ?><pre><?
        //print_r($request);?></pre><?
        return $request;

    }
*/
    public function loadRestorePass2( $idUser, $getId ) {
       /*  $_POST['00012FIRSTNAME'];
        $_POST['00012MIDDLENAME'];
        $_POST['00012LASTNAME'];
        $_POST['00012BIRTHDAY'];
        var_dump($FIRSTNAME1 );
        var_dump($MIDDLENAME1);
        var_dump($LATNAME1);
        var_dump($BIRTHDAY1);*/
        if ( $idUser === 1 ) {
            $q = [
                "IBLOCK_ID"     => 14,
                "ACTIVE"        => "Y",
                "ID"            => $getId
            ];
        } else {
            $q = [
                "IBLOCK_ID"     => 14,
                "ACTIVE"        =>"Y",
                "PROPERTY_USER" => $idUser,
                "ID"            => $getId
            ];
        }
        $res = CIBlockElement::GetList(
            [],
            $q,
            false,
            false,
            [
                "ID",
                "PROPERTY_COMPANY", // предприятие
                "PROPERTY_PASSES", // Привязка к заявке в хайлоаде
                "PROPERTY_type_passe", // Тип пропуска
                "PROPERTY_type_trip", // тип поездки
                "PROPERTY_INN", // инн
                "PROPERTY_KPP_IN", // кпп вьезда
                "PROPERTY_KPP_OUT", // квв вьезда
                "PROPERTY_CONTRACT", // Номер заказа(договора)
                "PROPERTY_CURATOR", // CURATOR
                "PROPERTY_where_issued", // тип поездки
                "PROPERTY_END_PASS", // тип поездки
                "PROPERTY_SIGN_SKIP", // тип поездки
                "PROPERTY_RECEIVING_UNIT", // тип поездки
                "PROPERTY_DATE_VISIT",
                "PROPERTY_ISSUE_PASSES",
                "PROPERTY_ORG_TEHNIC",
                //"PROPERTY_SPECIAL_OBJECT", // тип поездки
            ]
        );
        $request = [];
        while($arFields = $res->Fetch()) {
            // special object

            $curator = CIBlockElement::GetList(
                [],
                [
                    "IBLOCK_ID" => 20,
                    "ID"        => $arFields['PROPERTY_CURATOR_VALUE'],

                ],
                false,
                false,
                [
                    'NAME',
                    'PROPERTY_mail'
                ]
            );
            $arFields[ 'PROPERTY_CURATOR_VALUE_FULL' ] = $curator->Fetch();

            $specialObject = CIBlockElement::GetProperty(
                14,
                $getId,
                ["sort" => "asc"],
                ["CODE" => "SPECIAL_OBJECTS"]
            );
            while ($obSpecial = $specialObject->GetNext()) {
                $arFields["SPECIAL_OBJECTS"][] = $obSpecial['VALUE'];
            }

            $arFields['PROPERTY']["PASSES"] = $this->inHL2(
                2,
                [
                    "ID" => $arFields["PROPERTY_PASSES_VALUE"]
                ]
            );
            if ( $arFields['PROPERTY']["PASSES"][0]["UF_VISITOR"] != '' ) {

                $arFields['PROPERTY']["VISITOR"] = $this->inHL2(
                    12,
                    [
                        "ID" => $arFields['PROPERTY']["PASSES"][0]["UF_VISITOR"],
                        '?UF_FIRSTNAME' =>  $_POST['00012FIRSTNAME'],
                        '?UF_MIDDLENAME' => $_POST["00012MIDDLENAME"],
                        '?UF_LASTNAME' => $_POST["00012LASTNAME"],
                        '?UF_BIRTHDAY' => $_POST['00012BIRTHDAY']
                    ]
                );
                $arFields['PROPERTY']["VISITOR"][0]["UF_FILE_NAME"] = CFile::GetByID( $arFields['PROPERTY']["VISITOR"][0]["UF_FILE"] );
                $arFields['PROPERTY']["VISITOR"][0]["UF_FILE_NAME"] = $arFields['PROPERTY']["VISITOR"][0]["UF_FILE_NAME"]->arResult['0']["FILE_NAME"];
            }

            if ( $arFields['PROPERTY']["PASSES"][0]["UF_VEHICLE"] != '' ) {
                $arFields['PROPERTY']["VEHICLE"] = $this->inHL2(
                    5,
                    [
                        "ID" => $arFields['PROPERTY']["PASSES"][0]["UF_VEHICLE"]
                    ]
                );
                $arFields['PROPERTY']["VEHICLE"][0]["UF_OSAGO_FILE_NAME"] = CFile::GetByID( $arFields['PROPERTY']["VEHICLE"][0]["UF_OSAGO_FILE"] );
                $arFields['PROPERTY']["VEHICLE"][0]["UF_OSAGO_FILE_NAME"] = $arFields['PROPERTY']["VEHICLE"][0]["UF_OSAGO_FILE_NAME"]->arResult['0']["FILE_NAME"];

                $arFields['PROPERTY']["VEHICLE"][0]["UF_REG_FILE_NAME"] = CFile::GetByID( $arFields['PROPERTY']["VEHICLE"][0]["UF_REG_FILE"] );
                $arFields['PROPERTY']["VEHICLE"][0]["UF_REG_FILE_NAME"] = $arFields['PROPERTY']["VEHICLE"][0]["UF_REG_FILE_NAME"]->arResult['0']["FILE_NAME"];
            }
            // load parcking
            if ($arFields['PROPERTY']["VEHICLE"][0]["UF_PARKING"] != '') {
                $resParcing = CIBlockElement::GetList(
                    [],
                    [
                        "IBLOCK_ID" => 38,
                        "ACTIVE"=>"Y",
                        "ID" => $arFields['PROPERTY']["VEHICLE"][0]["UF_PARKING"]
                    ],
                    false,
                    false,
                    [
                        "ID",
                        "NAME",
                        "PROPERTY_AREA", // Площадка
                        "PROPERTY_UNIT_RESPONSIBLE", // Подразделение, ответственное за содержание
                        "PROPERTY_CAR_PARKING", // № автостоянки
                        "PROPERTY_SIGN_SKIP", // признак пропуска
                    ]
                );
                $arFields["PARCING"] = $resParcing->Fetch();
            }
            // loader driver
            if ( $arFields['PROPERTY']["PASSES"][0]["UF_DRIVERS"] != [] and $arFields['PROPERTY']["PASSES"][0]["UF_DRIVERS"] != '' ) {
                foreach ($arFields['PROPERTY']["PASSES"][0]["UF_DRIVERS"] as $key => $value) {
                    $arFields['PROPERTY']["DRIVER"][] = $this->inHL2(
                        4,
                        [
                            "ID" => $value,
                        ]
                    );

                }
            }

            if ( $arFields['PROPERTY']["PASSES"][0]["UF_PHOTOS"] != [] and $arFields['PROPERTY']["PASSES"][0]["UF_PHOTOS"] != '' ) {
                foreach ($arFields['PROPERTY']["PASSES"][0]["UF_PHOTOS"] as $key => $value) {
                    $arFields['PROPERTY']["PHOTO"][] = $this->inHL2(
                        11,
                        [
                            "ID" => $value
                        ]
                    );
                }
            }

            if ( $arFields['PROPERTY']["PASSES"][0]["UF_TMC"] != [] and $arFields['PROPERTY']["PASSES"][0]["UF_TMC"] != '' ) {
                foreach ($arFields['PROPERTY']["PASSES"][0]["UF_TMC"] as $key => $value) {
                    $arFields['PROPERTY']["TMC"][] = $this->inHL2(
                        15,
                        [
                            "ID" => $value
                        ]
                    );
                }
            }

            foreach ( $arFields['PROPERTY']["PASSES"][0]["UF_CONTRACTS"] as $key => $value) {
                $arFields['PROPERTY']["PASSES"][0]["UF_CONTRACTS_NAME"][] = CFile::GetByID( $value );
            }
            $request = $arFields;
        }

        return $request;
        //var_dump($request);
    }

}



$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_DATE_VISIT");
$arFilter = Array("IBLOCK_ID" => "14", "ACTIVE" => "Y", "?PROPERTY_DATE_VISIT" => $result2);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 300), $arSelect);
while ($ob = $res->Fetch()) {
    $result[] = $ob['ID'];
}
//echo var_dump($result);
$value = array();

foreach ($result as $value) {
    $model = new evraz3;
    $data = $model->loadRestorePass2(1, $value);
    $firstname[].= $data["PROPERTY"]["VISITOR"][0]["UF_FIRSTNAME"];



    //$filtered = array_filter($data["PROPERTY"]["VISITOR"][0]["UF_FIRSTNAME"], function($var){return !is_null($var);} );
    //echo var_dump($filtered);
    /* echo var_dump($data["PROPERTY"]["VISITOR"][0]["UF_MIDDLENAME"]);
    echo var_dump($data["PROPERTY"]["VISITOR"][0]["UF_LASTNAME"]);
    echo var_dump($data["PROPERTY"]["VISITOR"][0]["UF_BIRTHDAY"]);
    echo var_dump($data['PROPERTY']["DRIVER"][0]["UF_FIRSTNAME"]);
*/

}
$keys = array_keys($firstname,NULL);
if(!empty($keys))
{
    foreach($keys as $key)
    {
        unset($firstname[$key]);
    }
}
$CountElem = count($firstname, COUNT_RECURSIVE);
if($CountElem > "5"){
    ?>
    <script> alert("У вас больше 5 попыток");
        document.getElementById("btn2").style.display = "none";
    </script>

    <?

    CModule::IncludeModule('iblock');

    echo 'Вот такие данные мы передали';
    echo '<pre>';
   // print_r($_POST);
    print_r($CountElem);
    echo '<pre>';

    $el = new CIBlockElement;
    $iblock_id = 51;


    //Свойства

    $PROP = array();
    $PROP['FIRSTNAME'] = $_POST['00012FIRSTNAME']; //Свойство Строк
    $PROP['MIDDLENAME'] = $_POST["00012MIDDLENAME"]; //Свойство файл
    $PROP['LASTNAME'] =  $_POST["00012LASTNAME"];
    $PROP['id_user'] = $GLOBALS['USER']->GetID();
    $PROP['BIRTHDAY'] = $_POST['00012BIRTHDAY'];
    $PROP['count_trying'] = $CountElem;

    //Основные поля элемента
    $fields = array(
        "DATE_CREATE" => date("d.m.Y H:i:s"), //Передаем дата создания
        "CREATED_BY" => $GLOBALS['USER']->GetID(),    //Передаем ID пользователя кто добавляет
        "IBLOCK_ID" => $iblock_id,
        "PROPERTY_VALUES" => $PROP, // Передаем массив значении для свойств
        "NAME" => $PROP['id_user'],
        "ACTIVE" => "Y", //поумолчанию делаем активным или ставим N для отключении поумолчанию

    );


    //Результат в конце отработки
    if ($ID = $el->Add($fields)) {
        echo "Сохранено";
    } else {
        echo 'Произошел как-то косяк Попробуйте еще разок';
    }

}else{
    echo 'Вот такие данные мы передали';
    echo '<pre>';
    print_r($_POST);
    print_r($CountElem);
    echo '<pre>';
}